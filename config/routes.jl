using Genie.Router
using TraceMetasController
using HTTP

# Serve metadata
route("/", TraceMetasController.index)

route("/meta/:name", TraceMetasController.setmeta, method = POST)

route("/:type/:name", TraceMetasController.http_build, method = "PATCH")

route("/:type/json/:name", TraceMetasController.http_fetch, method = "GET")
