using Genie.Configuration

const config =  Settings(
                  output_length           = 100,
                  suppress_output         = false,
                  log_formatted           = true,
                  log_level               = :debug,
                  log_cache               = true,
                  log_views               = true,
                  assets_path             = "/",
                  cache_duration          = 0,
                  websocket_server        = false,
                  session_auto_start      = false,
                  cors_headers            = Dict(
                    "Access-Control-Allow-Origin"  => "*",
                    "Access-Control-Allow-Headers"  => "*",
                    "Access-Control-Allow-Methods" => "*",
                    "Access-Control-Max-Age"       => "86400"
                  )
                )

ENV["JULIA_REVISE"] = "auto"
