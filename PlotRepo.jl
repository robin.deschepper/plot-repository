module PlotRepo

Base.eval(Main, :(const UserApp = PlotRepo))

include("genie.jl")

Base.eval(Main, :(const Genie = PlotRepo.Genie))
Base.eval(Main, :(using Genie))

using Genie, Genie.Router, Genie.Renderer, Genie.AppServer

end
