module TraceMetasController
    using Thesis, DataStructures, Plots, JSON
    using HTTP
    using Genie.Router

    const DIR_D = "C:/Users/Robin/GIT/thesis_data/"

    function cors(status_code, content)
        HTTP.Response(status_code, [
            "X-Plot-Repo" => true,
            "Access-Control-Allow-Origin" => "*"
        ], body = content)
    end

    function read_meta()
        meta = Dict()
        open(DIR_D * "meta.json") do f
            meta = JSON.parse(read(f, String))
        end
        meta
    end

    function write_meta(meta)
        open(DIR_D * "meta.json", "w") do f
            write(f, JSON.json(meta))
        end
    end

    function index()
        meta = read_meta()
        nodes = Dict()
        for type in readdir(DIR_D)
            type_path = DIR_D * type
            if isdir(type_path) && type[1] != '.'
                for node in readdir(type_path)
                    node_name = splitext(basename(node))[1]
                    if !(node_name == "html" || node_name == "json")
                        if !haskey(nodes, node_name)
                            nodes[node_name] = []
                        end
                        push!(nodes[node_name], type)
                    end
                end
            end
        end
        for pair in nodes
            node = pair[1]
            has = pair[2]
            if !haskey(meta, node)
                meta[node] = Dict("hasType" => has)
            else
                meta[node]["hasType"] = has
            end
        end
        write_meta(meta)
        cors(200,JSON.json(Dict("meta" => SortedDict(meta), "types" => ["raw","filtered","annotated","deconvoluted","final","minimized"])))
    end

    function setmeta()
        name = string(@params(:name))
        payload = JSON.parse(@params(:RAW_PAYLOAD))
        meta = read_meta()
        for key in keys(payload)
            meta[name][key] = payload[key]
        end
        write_meta(meta)
        response = cors(200, JSON.json(Dict("message" => "Keys for '$name' updated.", "echo" => payload)))
    end

    function http_build()
        plotly()
        name = string(@params(:name))
        type = string(@params(:type))
        trace_type = getTraceType(type)
        if trace_type == "unknown"
            cors(400, JSON.json(Dict("message" => "Unknown trace type parameter '" * type * "'")))
        else
            trace = from_meta(trace_type, name)
            build(trace)
            compile(trace)
            cors(200, JSON.json(Dict("message" => "Successfully rebuilt " * name)))
        end
    end

    function http_fetch()
        txt = ""
        open("C:/Users/Robin/GIT/thesis_data/" * @params(:type) * "/json/" * @params(:name) * ".json") do f
            txt = read(f, String)
        end
        cors(200, txt)
    end
end
